# README

그저 NetStream을 개조했을 뿐임  
README에서 비틱냄새가 너무 심하게 나서 수정함  

20181126: disconnect() 함수랑 room() 함수 추가 자세한건 설명서로  

## 그래서 어떻게 쓰는데요

```as
import com.doctorChoi.cStream;
import com.doctorChoi.cStreamEvent;

var cs: cStream = new cStream("<방이름>", "<cirrus키>");
//객체 선언

cs.connect("<대상 peerID>");
//연결

cs.on(function (e: cStreamEvent): void
{
    //...
});
//이벤트

cs.send("<info>", "<data");
//전송
```

## 설명서

### cStream

```as
public function cStream(_room: String, _key: String, _host: String = "p2p.rtmfp.net"): void
```

cStream 객체를 만듭니다. 생성후 자동으로 연결되지 않습니다.  
성공적으로 생성하였을시 {type:cStream.READY} 이벤트가 발생합니다.  

```as
public function connect(fid: String): void
```

대상 pid에 연결합니다.  

```as
public function get myPeerID(): String
```

내 peerID를 반환합니다.

```as
public function get isConnected(): Boolean
```

연결되어있으면 true 아니면 false를 반환합니다.

```as
public function disconnect(): void
```

연결을 끊습니다.

```as
public function set room(_room: String): void
```

방 이름을 바꿉니다. 연결중이 아닐때만 바뀝니다.

```as
public function send(info: String, data: Object): void
```

데이터를 던집니다.

```as
public function on(func: Function): void
```

이벤트리스너를 달아줍니다.

```as
public function off(): void
```

이벤트리스너를 때냅니다.

### cStreamEvent

```as
public function cStreamEvent(type: String, info: Object, bubbles: Boolean = false, cancelable = false): void
```

cStreamEvent 객체를 만듭니다.

```as
public function get data(): Object
```

받은 데이터의 data 부분을 받아옵니다.

```as
public function get info(): String
```

받은 데이터의 type 부분을 가져옵니다.  